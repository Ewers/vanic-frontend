import React, {PropTypes} from 'react';
import {connect}   from 'react-redux';
import { browserHistory } from 'react-router';
import { bindActionCreators } from 'redux';
import * as userActions from '../redux/actions/userAction.js';
import axios from 'axios';
import { routeActions } from 'react-router-redux';


export default function requireAuthentication(Component) {

    class AuthenticatedComponent extends React.Component {

        static propTypes = {
            isAuthenticated: PropTypes.bool,
            actions: PropTypes.object.isRequired,
            routeAction: PropTypes.object.isRequired
        };

        componentWillMount() {
            this.checkAuth();
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth();
        }

        checkAuth(){

        }

        render() {
            return (
                <div>
                    {this.props.isAuthenticated === true
                        ? <Component {...this.props}/>
                        : null
                    }
                </div>
            );

        }
    }

    function mapDispatchToProps (dispatch) {
      return {
        actions: bindActionCreators(userActions, dispatch),
        routeAction: bindActionCreators(routeActions, dispatch)
      };
    }

    const mapStateToProps = (state) => ({
        isAuthenticated: state.user.isAuthenticated
    });

    return connect(mapStateToProps, mapDispatchToProps)(AuthenticatedComponent);

}
