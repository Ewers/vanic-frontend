import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';

// NOTE: here we're making use of the `resolve.root` configuration
// option in webpack, which allows us to specify import paths as if
// they were from the root of the ~/src directory. This makes it
// very easy to navigate to files regardless of how deeply nested
// your current file is.
import CoreLayout from 'layouts/CoreLayout';
import HomeView from 'views/HomeView/HomeView';
import NotFoundView from 'views/NotFoundView/NotFoundView';
import AppView from 'views/AppView/AppView';
import LoginView from 'views/LoginView/LoginView';
import RegisterView from 'views/RegisterView/RegisterView';
import NewTask from 'views/NewTask/NewTask';

export default (
  <Route path='/'>
    <IndexRoute name='Welcome' component={HomeView} />
    <Route name='Login' path='/login' component={LoginView} />
    <Route name='Register' path='/register' component={RegisterView}/>
    <Route path='/app' component={CoreLayout}>
    	<IndexRoute name='App' component={AppView} />
        <Route name='New project' path='/app/new' component={NewTask}/>
    </Route>
    <Route name='Not found' path='/404' component={NotFoundView} />
    <Redirect from='*' to='/404' />
  </Route>
);
