import * as types from '../constants/ActionTypes';
import axios from 'axios';
import Lokka from 'lokka';
import Transport from 'lokka-transport-http';
import { routeActions } from 'react-router-redux';
import {toggleSnackbar} from './miscActions';

const client = new Lokka({
  transport: new Transport('http://localhost:9000/graphql')
});

export function loginSuccess (data) {
  return { type: types.LOGINSUCCESS, data };
}

export function loginFailure (data) {
  return { type: types.LOGINFAILURE, data };
}

export function registerFailure (data) {
  return { type: types.REGISTERFAILURE, data };
}

export function registerSuccess (data) {
  return { type: types.REGISTERFAILURE, data };
}

export function postNewTask (title, project, desc, tags) {
  return dispatch => {
    const id = localStorage.getItem('userId');
    const query = `
      {
        project: addProject(
            title: "${title}",
            project: "${project}",
            description: "${desc}",
            tags: "${tags}",
            id: "${id}"
          ) {
            title
          }
      }
      `;
    client.mutate(query).then(result => {
      console.log(JSON.stringify(result));
      dispatch(toggleSnackbar('New task added!'));
    }).catch(result => {
      console.log(result);
    });
  };
}

export function register (email, username, password) {
  return dispatch => {
    axios.post('/auth/register', {
      email: email,
      username: username,
      password: password
    })
    .then(function (response) {
      dispatch(registerSuccess(response.data));
      dispatch(routeActions.push('/login'));
    })
    .catch(function (response) {
      console.log(response);
      dispatch(registerFailure(response.data));
    });
  };
}

export function login (username, password) {
  return dispatch => {
    axios.post('/auth', {
      username: username,
      password: password
    })
		.then(function (response) {
      console.log(response);
      localStorage.setItem('userId', response.data.userId);
      dispatch(loginSuccess(response.data));
      dispatch(toggleSnackbar('Logged in!'));
      dispatch(routeActions.push('/app'));
		})
		.catch(function (response) {
      dispatch(loginFailure(response.data));
		});
  };
}

export function logout () {
  return dispatch => {
    axios.get('/logout')
    .then(function (response) {
      localStorage.removeItem('userId');
      dispatch(routeActions.push('/'));
    })
    .catch(function (response) {
      console.log(response);
    });
  };
}
