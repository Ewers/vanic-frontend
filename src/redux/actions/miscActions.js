import * as types from '../constants/ActionTypes';

export function toggleSnackbar (message) {
  return { type: types.TOGGLESNACKBAR, message };
}

export function openmenu () {
  return { type: types.OPENMENU, };
}