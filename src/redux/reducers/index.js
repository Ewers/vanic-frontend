import { combineReducers } from 'redux';
import { routeReducer as router } from 'react-router-redux';
import user from './user';
import misc from './misc';

export default combineReducers({
  user,
  misc,
  router
});
