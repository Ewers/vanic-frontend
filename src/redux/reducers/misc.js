import {TOGGLESNACKBAR, OPENMENU} from '../constants/ActionTypes';
import {Map, fromJS} from 'immutable';

const initialState = Map({
	openSnackbar: false,
	message: '',
	openMenu: false
});

export default function user (state = initialState, action) {
  console.log(!state.get('openSnackbar'));
  switch (action.type) {
    case TOGGLESNACKBAR:
      return state.withMutations(state => {
        state.set('openSnackbar', fromJS(!state.get('openSnackbar'))).set('message', fromJS(action.message));
      });
    case OPENMENU:
      return state.set('openMenu', fromJS(!state.get('openMenu')));
    default:
      return state;
  }
}