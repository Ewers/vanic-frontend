import {LOGINSUCCESS, LOGINFAILURE, REGISTERFAILURE, REGISTERSUCCESS} from '../constants/ActionTypes';

import {Map, fromJS} from 'immutable';

export default function user (state = Map({}), action) {
  switch (action.type) {
    case LOGINSUCCESS:
      return state.withMutations(state => {
        state.set('isAuthenticated', action.data.isAuthenticated ? action.data.isAuthenticated : false)
          .set('successMessage', action.data.message && action.data.message)
          .set('userId', action.data.userId);
      });
    case LOGINFAILURE:
      return state.withMutations(state => {
        state.set('isAuthenticated', action.data.isAuthenticated ? action.data.isAuthenticated : false)
          .set('password', action.data.message.search('password') !== -1 && action.data.message)
          .set('username', action.data.message.search('user') !== -1 && action.data.message);
      });
    case REGISTERFAILURE:
      return state.withMutations(state => {
        state.set('isAuthenticated', action.data.isAuthenticated ? action.data.isAuthenticated : false)
          .set('email', action.data.messageaction.data.message.search('email') !== -1 && action.data.messageaction.data.message)
          .set('password', action.data.messageaction.data.message.search('password') !== -1 && action.data.message)
          .set('username', action.data.messageaction.data.message.search('username') !== -1 && action.data.message);
      });
    case REGISTERSUCCESS:
      return state.withMutations(state => {
        state.set('isAuthenticated', action.data.isAuthenticated ? action.data.isAuthenticated : false)
          .set('successMessage', action.data.message && action.data.message)
          .set('userId', action.data.userId);
      });
    default:
      return state;
  }
}
