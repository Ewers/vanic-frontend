import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import * as userActions from '../../redux/actions/userAction.js';
import * as miscActions from '../../redux/actions/miscActions.js';
import {TextField, RaisedButton, Snackbar, Card} from 'material-ui';
import { routeActions } from 'react-router-redux';

function mapDispatchToProps (dispatch) {
  return {
    routeAction: bindActionCreators(routeActions, dispatch)
  };
}

@connect(null,mapDispatchToProps)
export class HomeView extends React.Component {
  render () {
    return (
      <Card style={{padding: '3em'}}>
        <h1>Home page!</h1>
      </Card>
    );
  }
}

export default HomeView;
