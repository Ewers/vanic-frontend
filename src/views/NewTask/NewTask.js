import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../redux/actions/userAction.js';
import { routeActions } from 'react-router-redux';
import * as miscActions from '../../redux/actions/miscActions';
import {Card, TextField, RaisedButton,  Snackbar} from 'material-ui';

import styles from './newtask.styl';

const mapStateToProps = (state) => ({
  user: state.user,
  misc: state.misc
});

function mapDispatchToProps (dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    routeAction: bindActionCreators(routeActions, dispatch),
    miscActions: bindActionCreators(miscActions, dispatch)
  };
}

@connect(mapStateToProps, mapDispatchToProps)
class NewTask extends React.Component {

	static propTypes = {
		user: PropTypes.object.isRequired,
		misc: PropTypes.object.isRequired,
		routeAction: PropTypes.object.isRequired,
		userActions: PropTypes.object.isRequired
	};

	addTask = () => {
		const project = this.refs.project.getValue();
		const desc = this.refs.desc.getValue();
		const title = this.refs.title.getValue();
		const tags = this.refs.tags.getValue();
		const userId = this.props.user.userId;
		this.props.userActions.postNewTask(title, project, desc, tags, userId);
	    this.refs.project.clearValue();
	    this.refs.desc.clearValue();
	};

	render() {
		return (
		<section>
			<Card className={styles.container}>
				<h1>New task</h1>
			    <TextField
			      hintText='Title'
			      ref='title'
			      style={{marginRight: '1em'}}
			    />
			    <TextField
			      hintText='Project'
			      ref='project'
			    />
			    <TextField
			      hintText='Description'
			      ref='desc'
			      style={{marginRight: '1em'}}
			    />
			    <TextField
			      hintText='Tags'
			      ref='tags'
			    />
			    <br/>
			    <RaisedButton onClick={this.addTask} label='Send' primary />
			</Card>
		</section>
		);
	}
}

export default NewTask;
