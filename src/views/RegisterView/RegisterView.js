import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import * as userActions from '../../redux/actions/userAction.js';
import * as miscActions from '../../redux/actions/miscActions.js';
import {TextField, RaisedButton, Snackbar, Card} from 'material-ui';
import { routeActions } from 'react-router-redux';
import Helmet from 'react-helmet';

import styles from './register.styl';

const mapStateToProps = (state) => ({
  user: state.user,
  misc: state.misc
});



export class RegisterView extends React.Component {

  static propTypes = {
    user: PropTypes.object.isRequired,
    userActions: PropTypes.object.isRequired,
    miscActions: PropTypes.object.isRequired,
    routeAction: PropTypes.object.isRequired,
    misc: PropTypes.object.isRequired
  };

  handleRegister = (e) => {
    e.preventDefault();
    const password = this.refs.password.getValue();
    const username = this.refs.username.getValue();
    const email = this.refs.email.getValue();
    this.props.userActions.register(email, username, password);
    this.refs.password.clearValue();
    this.refs.username.clearValue();
    this.refs.email.clearValue();
  };

  render () {
    return (
      <div className={styles.container}>
      <Helmet
          title={`${this.props.routes[1].name} | VANIC`}
          meta={[
              {'name': 'description', 'content': 'Helmet application'},
              {'property': 'og:type', 'content': 'article'}
          ]}
      />
        <div className={styles.bg}></div>
        <Card className={styles.login}>
        	<h1>Register new user</h1>
          <form onSubmit={this.handleLogin}>
            <TextField
              fullWidth
              errorText={this.props.user.get('email')}
              floatingLabelText='Email'
              ref='email'
            />
            <TextField
            	fullWidth
              errorText={this.props.user.get('username')}
              floatingLabelText='Username'
              ref='username'
            />
            <TextField
              fullWidth
              errorText={this.props.user.get('password')}
              floatingLabelText='Password'
              type='password'
              ref='password'
            />
            {'  '}
            <RaisedButton className={styles.button} type='submit' onClick={this.handleRegister} label='Sudmit' primary />
            {'  '}
            <RaisedButton className={styles.button} onClick={() => this.props.routeAction.push('/login')} label='Login' primary />
          </form>
          <Snackbar
            open={this.props.misc.get('open')}
            message='Logged in!'
            action='close'
            onRequestClose={() => this.props.miscActions.toggleSnackbar()}
            autoHideDuration={3000}
          />
        </Card>
      </div>
    );
  }
}

function mapDispatchToProps (dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    miscActions: bindActionCreators(miscActions, dispatch),
    routeAction: bindActionCreators(routeActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterView);
