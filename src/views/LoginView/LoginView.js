import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import * as userActions from '../../redux/actions/userAction.js';
import * as miscActions from '../../redux/actions/miscActions.js';
import {TextField, RaisedButton, Snackbar, Card} from 'material-ui';
import { routeActions } from 'react-router-redux';
import Helmet from 'react-helmet';

import styles from './login.styl';

const mapStateToProps = (state) => ({
  user: state.user,
  misc: state.misc
});

function mapDispatchToProps (dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    miscActions: bindActionCreators(miscActions, dispatch),
    routeAction: bindActionCreators(routeActions, dispatch)
  };
}

@connect(mapStateToProps, mapDispatchToProps)
class LoginView extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    userActions: PropTypes.object.isRequired,
    miscActions: PropTypes.object.isRequired,
    misc: PropTypes.object.isRequired,
    routeAction: PropTypes.object.isRequired
  };

  componentWillMount() {
  	if (this.props.user.get('isAuthenticated')){
  		this.props.routeAction.push('/app');
  	}
  }

  handleLogin = (e) => {
    e.preventDefault();
    const password = this.refs.password.getValue();
    const username = this.refs.username.getValue();
    this.props.userActions.login(username, password);
    this.refs.password.clearValue();
    this.refs.username.clearValue();
  };

  render () {
    return (
      <div className={styles.container}>
      <Helmet
          title={`${this.props.routes[1].name} | VANIC`}
          meta={[
              {'name': 'description', 'content': 'Helmet application'},
              {'property': 'og:type', 'content': 'article'}
          ]}
      />
        <div className={styles.bg}></div>
        <Card className={styles.login}>
        	<h1>Login to your account</h1>
          <form onSubmit={this.handleLogin}>
            <TextField
            	fullWidth
              errorText={this.props.user.get('username')}
              floatingLabelText='Username'
              ref='username'
            />
            <TextField
              fullWidth
              errorText={this.props.user.get('password')}
              floatingLabelText='Password'
              type='password'
              ref='password'
            />
            {'  '}
            <RaisedButton className={styles.button} type='submit' onSubmit={this.handleLogin} label='Login' primary />
            {'  '}
            <RaisedButton className={styles.button} onClick={() => this.props.routeAction.push('/register')} label='Register' primary />
          </form>
          <Snackbar
            open={this.props.misc.openSnackbar}
            message='Logged in!'
            action='close'
            onRequestClose={() => this.props.miscActions.toggleSnackbar()}
            autoHideDuration={3000}
          />
        </Card>
      </div>
    );
  }
}

export default LoginView;