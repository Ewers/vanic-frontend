import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../redux/actions/userAction.js';
import { routeActions } from 'react-router-redux';
import {Card, CardHeader, CardText, CardActions, FlatButton} from 'material-ui';
import { resolve } from 'react-resolver';
import Lokka from 'lokka';
import Transport from 'lokka-transport-http';
const client = new Lokka({
  transport: new Transport('http://localhost:9000/graphql')
});

import styles from './app.styl';

const query = `
      query _($id: String) {
        user(id: $id) {
        	projects {
        		project
        		title
        		description
        		users {
        			username
        		}
        	}
        }
      }
`;

const mapStateToProps = (state) => ({
  userId: state.user.userId,
  misc: state.misc
});

function mapDispatchToProps (dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    routeActions: bindActionCreators(routeActions, dispatch),
  };
};

@connect(mapStateToProps, mapDispatchToProps)
@resolve('data', (props) => {
	const {routeActions} = props;
	const userId = localStorage.getItem('userId');
	if(userId !== null){
		return client.query(query, {id: userId});
	}
})
class Task extends React.Component {

	static propTypes = {
		data: PropTypes.object
	};

    shouldComponentUpdate(nextProps, nextState) {
        if(nextProps.data){
            return false;
        }
        return true;
    }

	render() {
		const True = true;
		console.log(this.props.data.user);
		return (
			<div className={styles.app}>
				{this.props.data.user.projects.map( (project, index) => {
					return(
						<Card key={index} className={styles.card}>
							<CardHeader
							  title={project.project}
							  subtitle={project.title}
							  actAsExpander={True}
							  showExpandableButton={True}
							/>
							<CardText expandable={True}>
								{project.users.map( (user, i) => <p key={i}> {user.username} </p> )}
							</CardText>
							<CardActions expandable={True}>
							  <FlatButton label='Action1'/>
							  <FlatButton label='Action2'/>
							</CardActions>
						</Card>);
				})}
			</div>
		);
	}
}

export default Task;
