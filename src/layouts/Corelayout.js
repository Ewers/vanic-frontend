import React, { PropTypes } from 'react';
import Header from './Header';
import Footer from './Footer';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {Snackbar} from 'material-ui';
import Helmet from 'react-helmet';
import * as miscActions from '../redux/actions/miscActions.js';

import '../styles/core.styl';

import styles from './core.styl';

function mapDispatchToProps (dispatch) {
  return {
    miscAction: bindActionCreators(miscActions, dispatch)
  };
};

const mapStateToProps = (state) => ({
  misc: state.misc
});

@connect(mapStateToProps, mapDispatchToProps)
class Corelayout extends React.Component {

  static propTypes = {
    miscAction: PropTypes.object.isRequired,
    misc: PropTypes.object.isRequired
  };

  handleToggle = () => this.props.miscAction.openmenu();

	render() {
		return (
		    <div className={styles.core}>
            <Helmet
                title={`${this.props.routes[2].name} | VANIC`}
                meta={[
                    {'name': 'description', 'content': 'Helmet application'},
                    {'property': 'og:type', 'content': 'article'}
                ]}
            />
		    	<Header />
          <main className={styles.main + ' ' + ((this.props.misc.get('openMenu') && this.props.misc.get('openMenu')) ? styles.mainOpen : '')}>
            {this.props.children}
          </main>
		      <Footer />
          <Snackbar
          open={this.props.misc.get('openSnackbar')}
          message={this.props.misc.get('message')}
          action='close'
          onRequestClose={() => this.props.miscAction.toggleSnackbar('')}
          autoHideDuration={3000}
          />
		    </div>
		);
	}
}

Corelayout.propTypes = {
  children: PropTypes.element
};

export default Corelayout;
