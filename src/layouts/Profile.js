import React, {PropTypes} from 'react';
import {ToolbarTitle} from 'material-ui';
import { resolve } from 'react-resolver';
import Lokka from 'lokka';
import Transport from 'lokka-transport-http';
const client = new Lokka({
  transport: new Transport('http://localhost:9000/graphql')
});
import styles from './header.styl';

const query = `
      query _($id: String) {
        user(id: $id) {
          username
        }
      }
`;
@resolve('data', (props) => {
  const {routeActions} = props;
  const userId = localStorage.getItem('userId');
  if(userId !== null){
    return client.query(query, {id: userId});
  }
})
class Profile extends React.Component {

	static propTypes = {
		data: PropTypes.object.isRequired
	};

  shouldComponentUpdate(nextProps, nextState) {
  	return this.props.data !== nextProps.data;
  }

	render() {
		return (
			<ToolbarTitle text={this.props.data.user.username} style={{color: '#fff', lineHeight: '48px', top: '-6px'}} />
		);
	}
}

export default Profile;
