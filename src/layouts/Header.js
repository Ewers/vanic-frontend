import React, {PropTypes} from 'react';
import {AppBar, ToolbarTitle, MenuItem, Avatar, IconMenu, IconButton, ListItem, List} from 'material-ui';
import ActionGrade from 'material-ui/lib/svg-icons/action/grade';
import SocialPerson from 'material-ui/lib/svg-icons/social/person';
import ActionInfo from 'material-ui/lib/svg-icons/action/info';
import ContentInbox from 'material-ui/lib/svg-icons/content/inbox';
import ContentDrafts from 'material-ui/lib/svg-icons/content/drafts';
import ContentSend from 'material-ui/lib/svg-icons/content/send';
import MoreVertIcon from 'material-ui/lib/svg-icons/navigation/more-vert';
import { routeActions } from 'react-router-redux';
import * as userActions from '../redux/actions/userAction.js';
import * as miscActions from '../redux/actions/miscActions.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './header.styl';
import Profile from './Profile';

const mapStateToProps = (state) => ({
  misc: state.misc
});

function mapDispatchToProps (dispatch) {
  return {
    routeAction: bindActionCreators(routeActions, dispatch),
    miscAction: bindActionCreators(miscActions, dispatch),
    userAction: bindActionCreators(userActions, dispatch)
  };
}

@connect(mapStateToProps, mapDispatchToProps)
class Header extends React.Component {

  static propTypes = {
    userId: PropTypes.object,
    miscAction: PropTypes.object.isRequired,
    misc: PropTypes.object.isRequired,
    routeAction: PropTypes.object.isRequired,
    userAction: PropTypes.object.isRequired
  };

  handleToggle = () => this.props.miscAction.openmenu();

  shouldComponentUpdate(nextProps, nextState) {
    return (this.props.misc !== nextProps.misc);
  }

  render () {
    return (
      <header>
        <AppBar
          title='Vanic'
          onLeftIconButtonTouchTap={this.handleToggle}
          style={{boxShadow: '0px'}}
          iconStyleRight={{marginTop: '0px'}}
          iconElementRight={
            <div className={styles.rightIcon}>
              <IconButton  tooltip='Profile' touch>
                <SocialPerson color='#0d47a1' />
              </IconButton>
              <Profile id={this.props.userId} />
              <IconMenu
                iconButtonElement={
                  <IconButton><MoreVertIcon /></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              >
                <MenuItem primaryText='Help' />
                <MenuItem onTouchTap={() => this.props.userAction.logout()} primaryText='Sign out' />
              </IconMenu>
            </div>
          }
        />
        <section className={styles.leftNav + ' ' + (this.props.misc.get('openMenu') ? styles.leftNavOpen : '')}>
            <List subheader='Menu' style={{backgroundColor: '#f6f6f6'}}>
              <ListItem onTouchTap={() => this.props.routeAction.push('/app')} primaryText='Dashboard' leftIcon={<ActionGrade />} />
              <ListItem onTouchTap={() => this.props.routeAction.push('/app/new')} primaryText='New' leftIcon={<ContentDrafts />} />
              <ListItem primaryText='Sent mail' leftIcon={<ContentSend />} />
              <ListItem
                primaryText='Current'
                leftIcon={<ContentInbox />}
                initiallyOpen={false}
                primaryTogglesNestedList
                nestedListStyle={{backgroundColor: '#f6f6f6'}}
                nestedItems={[
                  <ListItem
                    key={1}
                    primaryText='Starred'
                    leftIcon={<ActionGrade />}
                  />,
                  <ListItem
                    key={2}
                    primaryText='Sent Mail'
                    leftIcon={<ContentSend />}
                  />
                ]}
              />
            </List>
        </section>
      </header>
    );
  }
}

export default Header;